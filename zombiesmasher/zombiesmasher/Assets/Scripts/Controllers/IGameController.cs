﻿using UnityEngine;
using System.Collections;

public interface IGameController
{
    void Enable();
    void Disable();
    void Update();
}
