﻿using System;
using UnityEngine;
using System.Collections;

public class GameController : IGameController
{
    public Action<int> KillAction;
    public Action KillFriendlyAction;
    public Action<int> WaveChangedAction;

    public float gameStartedTime;

    private int killCount;
    private int killFriendlyCount;

    private GameView _view;
    private WaveManager _waveManager;



    public void Enable()
    {
        _view = GameObject.FindObjectOfType<GameView>();
        _view.Show();

        _waveManager = GameObject.FindObjectOfType<WaveManager>();
        _waveManager.WaveChangedAction += WaveChangedAction;
        
        StartGame();
    }

    public void Disable()
    {
        _view.Hide();

        _waveManager.StopLevel();
        _waveManager.WaveChangedAction -= WaveChangedAction;
        _waveManager = null;
        KillAction = null;
        KillFriendlyAction = null;
    }

    public void KillEnemy(Enums.EnemyType type)
    {
        if (type == Enums.EnemyType.Friendly)
        {
            killFriendlyCount++;
            
            if (killFriendlyCount >= 3)
            {
                StopGame();
            }

            if(KillFriendlyAction != null)
                KillFriendlyAction();
        }
        
        killCount++;
        if (KillAction != null)
        {
            KillAction(killCount);
        }
    }

    private void StartGame()
    {
        Debug.Log("StartGame");
        killCount = 0;
        killFriendlyCount = 0;
        gameStartedTime = Time.time;
        _waveManager.StartLevel();
        KillAction(killCount);
    }

    private void StopGame()
    {
        GameManager.Instance.LoadScene(Enums.GameStates.GameOver);
    }

    public void Update()
    {
        if (Time.time - gameStartedTime >= 59)
        {
            StopGame();
        }
    }
}
