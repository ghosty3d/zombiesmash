﻿using UnityEngine;
using System.Collections;

public class MainMenuController : IGameController
{
    private MainMenuView _view;

    public void Enable()
    {
        _view = GameObject.FindObjectOfType<MainMenuView>();
        _view.AboutAction += AboutActionHandler;
        _view.PlayAction += PlayActionHandler;
        _view.Show();

    }

    public void Disable()
    {
        _view.Hide();
        _view.AboutAction -= AboutActionHandler;
        _view.PlayAction -= PlayActionHandler;
    }

    private void AboutActionHandler()
    {
        Debug.Log("About");
    }

    private void PlayActionHandler()
    {
        GameManager.Instance.LoadScene(Enums.GameStates.Game);
    }

    public void Update()
    {
        
    }
}
