﻿using UnityEngine;
using System.Collections;

public class GameOverController : IGameController
{
    private GameOverView _view;

    public void Enable()
    {
        _view = GameObject.FindObjectOfType<GameOverView>();
        _view.RestartAction += RestartHandler;
        _view.MainMenuAction += MainMenuHandler;
        _view.Show();
    }

    private void MainMenuHandler()
    {
        GameManager.Instance.LoadScene(Enums.GameStates.MainMenu);
    }

    private void RestartHandler()
    {
        GameManager.Instance.LoadScene(Enums.GameStates.Game);
    }

    public void Disable()
    {
        _view.Hide();
        _view.RestartAction -= RestartHandler;
        _view.MainMenuAction -= MainMenuHandler;
        _view = null;
    }

    public void Update()
    {

    }
}
