﻿using UnityEngine;
using System.Collections;

public class Enums
{
    public enum GameStates : int
    {
        MainMenu = 0,
        Game,
        GameOver
    }

    public enum EnemyType : int
    {
        Friendly = 0,
        Crawl,
        Walk,
        Run
    }
 }
