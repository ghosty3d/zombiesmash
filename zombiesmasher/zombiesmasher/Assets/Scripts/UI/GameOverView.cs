﻿using System;
using UnityEngine;
using System.Collections;

public class GameOverView : BaseView
{
    public Action RestartAction;
    public Action MainMenuAction;

    public void OnRestartClick()
    {
        if (RestartAction != null)
            RestartAction();
    }

    public void OnMainMenuClick()
    {
        if (MainMenuAction != null)
            MainMenuAction();
    }
}
