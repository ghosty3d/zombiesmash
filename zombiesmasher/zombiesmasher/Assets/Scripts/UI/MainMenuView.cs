﻿using UnityEngine;
using System;

public class MainMenuView : BaseView
{
    public Action AboutAction;
    public Action PlayAction;


    public void OnAboutClick()
    {
        if(AboutAction != null)
            AboutAction();
    }

    public void OnPlayClick()
    {
        if (PlayAction != null)
            PlayAction();
    }
}
