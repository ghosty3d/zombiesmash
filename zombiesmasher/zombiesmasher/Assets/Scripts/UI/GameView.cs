﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class GameView : BaseView
{
    [SerializeField]
    private Text killCountLabel;

    [SerializeField]
    private Text waveNumberLabel;

    [SerializeField]
    private Text timeLabel;

    [SerializeField]
    private Transform _heartsParent;

    private GameController _gameController;

    override public void Show()
    {
        base.Show();
        
        _gameController = GameManager.Instance.CurrentController as GameController;
        _gameController.KillAction += EnemyKillHandler;
        _gameController.WaveChangedAction += WaveChangedHandler;
        _gameController.KillFriendlyAction += FriendlyKillHandler;
    }

    void OnDisable()
    {
        _gameController.KillAction -= EnemyKillHandler;
        _gameController.WaveChangedAction -= WaveChangedHandler;
        _gameController.KillFriendlyAction -= FriendlyKillHandler;
        _gameController = null;
    }

    private void EnemyKillHandler(int killCount)
    {
        killCountLabel.text = "Killed: " + killCount;
    }

    private void FriendlyKillHandler()
    {
        if (_heartsParent.childCount > 0)
        {
            Destroy(_heartsParent.GetChild(0).gameObject);
        }
    }

    private void WaveChangedHandler(int waveIndex)
    {
        waveNumberLabel.text = "Wave: " + (waveIndex + 1);
    }

    void Update()
    {
        if(_gameController != null)
        { 
            timeLabel.text = "Time: " + Mathf.RoundToInt(60 - (Time.time - _gameController.gameStartedTime));
            
        }
    }

}
