﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class WaveManager : MonoBehaviour
{
    public Action<int> KillAction;
    public Action<int> WaveChangedAction;

    [SerializeField]
    private Wave _currentWave;

    public List<Wave> Waves;

    private bool _isStarted;
    private float _waveTimer;
    private float _instantiateTimer;
    private float _timeToInstantiate;

    [SerializeField]
    private List<PrefabByType> _prefabs;

    void Update()
    {
        if (_isStarted)
        {
            if (_waveTimer >= 0)
            {
                _waveTimer -= Time.deltaTime;
                UpdateCurrentWave();
            }
            else
            {
                GetNextWave();
            }
        }
    }

    private void UpdateCurrentWave()
    {
        if (_instantiateTimer >= 0)
        {
            _instantiateTimer -= Time.deltaTime;
        }
        else
        {
            _instantiateTimer = _timeToInstantiate;
            GetRandomEnemy();
        }
    }

    private void GetRandomEnemy()
    {
        if (_currentWave.Elements.Count > 0)
        {
            var randElement = UnityEngine.Random.Range(0, _currentWave.Elements.Count);
            _currentWave.Elements[randElement].count--;

            //Get prefab
            var prefByType = _prefabs.Find(q => q.type == _currentWave.Elements[randElement].type);
            if (prefByType != null && prefByType.prefab != null)
            {
                InstantiateEnemy(prefByType.prefab, prefByType.type);
            }

            if (_currentWave.Elements[randElement].count <= 0)
            {
                _currentWave.Elements.RemoveAt(randElement);
            }
        }
    }

    private void InstantiateEnemy(GameObject prefab, Enums.EnemyType type)
    {
        //get random pos
        var pos = new Vector3(UnityEngine.Random.Range(-2, 2), 0, -5);

        //Instantiate
        var go = Instantiate(prefab, pos, Quaternion.identity) as GameObject;
        go.GetComponent<Enemy>().type = type;
    }

    public void StartLevel()
    {
        ResetLevel();
        _isStarted = true;
    }

    public void StopLevel()
    {
        _isStarted = false;
        _currentWave = null;
    }

    private void ResetLevel()
    {
        InitWave(0);
    }

    public void GetNextWave()
    {
        InitWave(Mathf.Min(_currentWave.waveNumber + 1, Waves.Count - 1));
    }

    private void InitWave(int waveIndex)
    {
        _instantiateTimer = 0;
        int waveEnemiesCount = 0;

        //Duplicate wave data to current wave
        var wave = Waves[waveIndex];
        _currentWave = new Wave();
        _currentWave.Elements = new List<Wave.WaveElement>();
        _currentWave.waveTime = wave.waveTime;
        _currentWave.waveNumber = Waves.IndexOf(wave);
        
        
        for (int i = 0; i < wave.Elements.Count; i++)
        {
            _currentWave.Elements.Add(new Wave.WaveElement() { type = wave.Elements[i].type, count = wave.Elements[i].count});
            waveEnemiesCount += wave.Elements[i].count;
        }

        _timeToInstantiate = _currentWave.waveTime / waveEnemiesCount;

        _waveTimer = _currentWave.waveTime;

        WaveChangedAction(waveIndex);
    }
}

[Serializable]
public class Wave
{
    public int waveNumber;
    public float waveTime;
    public List<WaveElement> Elements;

    [Serializable]
    public class WaveElement
    {
        public Enums.EnemyType type;
        [Range(0, 20)]
        public int count = 1;
    }
}

[Serializable]
public class PrefabByType
{
    public Enums.EnemyType type;
    public GameObject prefab;
}