﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance
    {
        get 
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();
            }

            return _instance;
        }
    }

    private static GameManager _instance;

    private Enums.GameStates _targetLevelId;
    public IGameController CurrentController
    {
        get;
        private set;
    }

    private bool _initialized;

    [SerializeField]
    private SpriteRenderer _loadingSprite;
    private Color _fadeAlphaColor = new Color(0, 0, 0, 0.05f);

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            Init();
        }
    }

    private void Init()
    {
        if (!_initialized)
        {
            _initialized = true;
            CreateSceneController(Enums.GameStates.MainMenu);
            DontDestroyOnLoad(gameObject);
        }
    }

    void Update()
    {
        if (CurrentController != null)
            CurrentController.Update();
    }


    public void LoadScene(Enums.GameStates targetLevelId)
    {
        CurrentController.Disable();
        CurrentController = null;

        _targetLevelId = targetLevelId;

        _loadingSprite.enabled = true;
        StartCoroutine(FadeLoadingSprite(true));
    }

    private IEnumerator FadeLoadingSprite(bool fade)
    {
        while (true)
        {
            if (fade)
            {
                _loadingSprite.color += _fadeAlphaColor;
                if (_loadingSprite.color.a >= 1)
                {
                    Application.LoadLevelAsync((int)_targetLevelId);
                    break;
                }
            }
            else
            {
                _loadingSprite.color -= _fadeAlphaColor;
                if(_loadingSprite.color.a <= 0)
                {
                    CreateSceneController(_targetLevelId);
                    _loadingSprite.enabled = false;
                    break;
                }
            }
            yield return new WaitForEndOfFrame();
        }
        
    }

    private void OnLevelWasLoaded(int level)
    {
        if (_instance == this)
        {
            StartCoroutine(FadeLoadingSprite(false));
        }
    }


    private void CreateSceneController(Enums.GameStates stateId)
    {
        switch (stateId)
        {
            case Enums.GameStates.MainMenu:
                CurrentController = new MainMenuController();
                break;
            case Enums.GameStates.Game:
                CurrentController = new GameController();
                break;
            case Enums.GameStates.GameOver:
                CurrentController = new GameOverController();
                break;
            default:
                break;
        }

        if(CurrentController != null)
            CurrentController.Enable();
    }

}
