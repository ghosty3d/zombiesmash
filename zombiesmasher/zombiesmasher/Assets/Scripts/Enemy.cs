﻿using System;
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CapsuleCollider))]
public class Enemy : MonoBehaviour
{
    public Enums.EnemyType type;

    [SerializeField]
	private Animator EnemyAnimator;
    [SerializeField]
    private CapsuleCollider EnemyCollider;

    [SerializeField]
    private float MoveSpeed = 1f;

    [SerializeField]
    private bool isDead = false;

	// Use this for initialization
	void Start ()
	{
		if(!EnemyAnimator)
		{
			EnemyAnimator = GetComponent<Animator>();
		}

		if(!EnemyCollider)
		{
			EnemyCollider = GetComponent<CapsuleCollider>();
		}

		EnemyCollider.isTrigger = true;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(!isDead)
		{
			transform.Translate(transform.forward * MoveSpeed * Time.deltaTime);
            if (transform.position.z > 5)
            {
                isDead = true;
                Destroy(gameObject);
            }
		}
	}

	void OnMouseDown()
	{
		if(!isDead)
		{
            (GameManager.Instance.CurrentController as GameController).KillEnemy(type);

			EnemyAnimator.SetTrigger("Die");
			isDead = true;
			Destroy(gameObject, 6f);
		}
	}
}
